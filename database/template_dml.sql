/*
SQLyog Ultimate v8.82 
MySQL - 5.1.43-community : Database - springwebtemplatedb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`springwebtemplatedb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `springwebtemplatedb`;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`permission_name`) values (1,'PRS_LOG'),(2,'PRS_CREATE_USER'),(3,'PRS_ADM_UPDATE_USER'),(4,'PRS_DISABLE_USR'),(5,'PRS_ENABLE_USR'),(6,'PRS_UPDATE_USR'),(8,'PRS_USER_LIST'),(9,'PRS_USR_VIEW'),(11,'PRS_DETAIL_USR'),(12,'PRS_CONFIG');

/*Data for the table `role_permission` */

insert  into `role_permission`(`role`,`permission`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,8),(1,9),(1,10),(1,11),(1,12),(2,1),(2,6),(2,9),(2,10);

/*Data for the table `roles` */

insert  into `roles`(`id`,`role_name`) values (1,'ROLE_ADMIN'),(2,'ROLE_REGULAR');

/*Data for the table `user_role` */

insert  into `user_role`(`user`,`role`) values (1,1),(2,1);

/*Data for the table `users` */

insert  into `users`(`id`,`user_name`,`password`,`created_date`,`enabled`) values (1,'admin','5f4dcc3b5aa765d61d8327deb882cf99','2011-03-28 21:06:29',1),(2,'adi','c46335eb267e2e1cde5b017acb4cd799','2011-03-14 02:33:29',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
