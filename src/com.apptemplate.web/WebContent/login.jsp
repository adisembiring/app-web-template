<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
</head>
<body>

<div class="article">
 	<h2><span>Login</span></h2><div class="clr"></div>
	<c:if test="${not empty param.error}">
		<font color="red"> Invalid username or password ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </font>
	</c:if>
	<form action="<c:url value="/j_spring_security_check" />" name="signinform" method="post">
	  <ol><li>
	    <label>Your Username</label>
	    <input type="text" name="j_username" value="" />
	  </li><li>
	    <label>Your Password</label>
	    <input type="password" name="j_password" value="" />
	  </li>
	  
	  <li>
	    <input type="submit" value="Login" /> <input type="reset" value="Reset" />
	    <div class="clr"></div>
	  </li></ol>
	  </form>
</div> 

</body>
</html>